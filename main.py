import logging
import telegram
from telegram.ext import MessageHandler, Filters
from telegram.ext import Updater

updater = Updater(token='')
dispatcher = updater.dispatcher
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)


def delete(bot: telegram.Bot, update: telegram.Update):
    message: telegram.Message = update.message
    user: telegram.User = update.effective_user
    bot.send_message(chat_id=user.id, text="Please be mindful of what you share with the group.")
    bot.delete_message(chat_id=message.chat_id, message_id=message.message_id)


msg = MessageHandler(Filters.contact |
                     Filters.regex('.*profit.*') |
                     Filters.regex('.*sex.*') |
                     Filters.regex(".*\d\d\d\%.*"), delete);

dispatcher.add_handler(msg)
updater.start_polling()
